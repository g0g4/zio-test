package zio.test.phonebook

import zio._
import zio.config.ReadError
import zio.config.magnolia.descriptor
import zio.config.typesafe.TypesafeConfig

package object configuration {

  case class Config(api: Api, liquibase: LiquibaseConfig, db: DbConfig)
  
  case class LiquibaseConfig(changeLog: String)
  case class Api(host: String, port: Int)
  case class DbConfig(driver: String, url: String, user: String, password: String)

  val configDescriptor = descriptor[Config]

  type Configuration = zio.Has[Config]
  
  object Configuration{
    val live: Layer[ReadError[String], Configuration] = TypesafeConfig.fromDefaultLoader(configDescriptor)
  }
}

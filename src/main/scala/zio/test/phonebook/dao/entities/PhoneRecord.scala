package zio.test.phonebook.dao.entities


case class PhoneRecord(id: String, phone: String, fio: String)

case class Address(id: String, zipCode: String, streetAddress: String)